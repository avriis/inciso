
# Definerer serier, der hentes data for
# serier <- c("IDD","WEALTH","TAXBEN","FIXINCLSA",
#             "BLI2013","BLI2014","BLI2015",
#             "GENDER_EMP","CWB")
serier <- c("WEALTH","IDD")
# countries <- "AUS+AUT+BEL+CAN+CHL+CZE+DNK+EST+FIN+FRA+DEU+GRC+HUN+ISL+IRL+ISR+ITA+JPN+KOR+LUX+MEX+NLD+NZL+NOR+POL+PRT+SVK+SVN+ESP+SWE+CHE+TUR+GBR+USA+RUS"
countries0 <- c("AUS","AUT","BEL","CAN")
countries <- paste(countries0,collapse = "+")
# years <- 2008:2014
years <- 2010

x <- list()
for(s in serier){
  other.vars <- switch (s,
                        "IDD" = paste("TOT",
                                      "CURRENT",
                                      "METH2011",sep = "."),
                        "WEALTH" = "TP"
  )
  structure <- OECD::get_data_structure(s)
  if(is.null(structure$MEASURE$id)){
    # IDD bruger MEASURE, WEALTH bruger VAR
    measures <- structure$VAR$id
  } else {
    measures <- structure$MEASURE$id
  }
  cat("Har fundet følgende mål:",measures,"\n")
  measures <- measures[1:10] # Skal slettes i endelig udgave
  # matrice <- matrix(nrow = length(countries0),
  #             ncol = length(measures))
  for(y in years){
    cat("Vi ser på følgende år:",y,"\n")
    counter <- 0
    for(m in measures){
      cat("Vi ser på følgende measure:",m,"\n")
      counter <- counter + 1
      if(is.null(other.vars)){
        filter <- paste(countries,m,sep = ".")
      } else {
        filter <- paste(countries,m,other.vars,sep = ".")
      }
      cat("Bruger følgende filter:",filter,"\n")
      df <- OECD::get_dataset(s,
                              filter = filter,
                              start_time = y,
                              end_time = y,
                              pre_formatted = TRUE)
      cat("Fandt følgende data i første omgang:")
      print(df);cat("\n")
      # print(df);stop("min fejl")
      if(counter == 1){
        df2 <- data.frame(df$LOCATION)
      }
      if(length(df$obsValue) != 0){
        df3 <- data.frame(df$obsValue)
        colnames(df3) <- m
        df2 <- cbind(df2,df3)
      }
    } # Slut på mål-loop
    # Giver række-navne
    rownames(df2) <- df2$df.LOCATION
    df2$df.LOCATION <- NULL
    # Gemmer data for individuelle år
    x[[as.character(y)]] <- df2
  } # Slut på års-loop
  # Combine data frames for individual years
} # Slut på serie-loop

for(y in years){
  cat("Vi ser på følgende år:",y,"\n")
  for(s in serier){
    counter <- 0
    for(m in measures){
      cat("Vi ser på følgende measure:",m,"\n")
      counter <- counter + 1
      if(is.null(other.vars)){
        filter <- paste(countries,m,sep = ".")
      } else {
        filter <- paste(countries,m,other.vars,sep = ".")
      }
      cat("Bruger følgende filter:",filter,"\n")
      df <- OECD::get_dataset(s,
                              filter = filter,
                              start_time = y,
                              end_time = y,
                              pre_formatted = TRUE)
      cat("Fandt følgende data i første omgang:")
      print(df);cat("\n")
      # print(df);stop("min fejl")
      if(counter == 1){
        df2 <- data.frame(df$LOCATION)
      }
      if(length(df$obsValue) != 0){
        df3 <- data.frame(df$obsValue)
        colnames(df3) <- m
        df2 <- cbind(df2,df3)
      }
    } # Slut på mål-loop
    # Læg data sammen for de enkelte serier
    df4 <- cbind(df4,df2)
  } # Slut på serie-loop
  # Giver række-navne
  rownames(df2) <- df2$df.LOCATION
  df2$df.LOCATION <- NULL
  # Gemmer data for individuelle år
  x[[as.character(y)]] <- df2
} # Slut på års-loop

## Kører diverse tests ##
# stopifnot(x$`2009`["BEL","GINI"] == 0.269)
x


# for(s in serier){
#   x <- OECD::get_dataset("IDD")
#   assign(s,
#          x)
# }



# # Definerer serie-titel
# serie <- "PAG"
# # OECD::browse_metadata(serie) # Her bliver man ledt til en browser, der viser dokumentation
# # Henter data
# df <- OECD::get_dataset(serie,
#                         filter = list(c("BEL", "FRA")),
#                         start_time = 2008, end_time = 2014)
# # Viser de første ti observationer
# head(df)